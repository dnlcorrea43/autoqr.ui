
app.$vuetify.icons.dropdown = 'fas fa-angle-down';
app.$vuetify.icons.clear = 'fas fa-times';
app.$vuetify.icons.expand = 'fas fa-angle-down';

app.$vuetify.theme.primary = '#7fbc07';
app.$vuetify.theme.secondary = {
    base: '#0b396d',
    darken1: '#09254a',
    lighten1: '#0064a9',
};
app.$vuetify.theme.accent = '#c0eb48';
app.$vuetify.theme.success = '#0163aa';
app.$vuetify.theme.info = '#707a84';
app.$vuetify.theme.tealpos = '#7fbc07';
app.$vuetify.theme.bluepos = '#0b396d';
app.$vuetify.theme.unidades = '#048e68';
app.$vuetify.theme.error = {
  base: '#E64A19',
}
