const mix = require('laravel-mix');

mix.js('resources/js/bootstrap.js', 'public/js')
    .js('resources/js/inscricoes.js', 'public/js')
    .js('resources/js/tema.js', 'public/js');

