<?php

namespace App\Http\Controllers\Resources;

use App\Inscricao;
use App\Mail\InscricaoMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class InscricoesController extends Controller
{

    public function store(Request $request)
    {
        $inscricao = Inscricao::create($request->all());

        QrCode::format('png')
            ->size(200)
            ->generate(
                json_encode($request->only(['ra', 'palestra_id'])),
                storage_path('/app/public/' . $request->ra . '.png')
            );

        Mail::to($request->email)->send(new InscricaoMail($inscricao));

        return ['status' => 'ok'];
    }
}
