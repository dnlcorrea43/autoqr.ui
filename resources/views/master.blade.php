<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('head')
    
    <body>

	<div id="app">

	    <v-app>
		@yield('content')
	    </v-app>
	    
	</div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/vuetify/1.5.14/vuetify.min.js"></script>
	@stack('js')

	<script src="/js/tema.js"></script>
    </body>
</html>
