<p>Olá,</p>

<p>Sua inscrição no Evento foi realizada com sucesso.</p>

<p>RA: {{ $inscricao->ra }}</p>

<p>Seu QR Code segue abaixo. Passe ele no leitor na tela de seu celular ou impresso.</p>

<p>
    <img src="{{ url('storage/' . $inscricao->ra . '.png') }}" />
</p>

<p>Atenciosamente,</p>

<p>Equipe UniAmérica.</p>

<p>
    <img style="width: 250px; height:auto" alt="Centro Universitário UniAmérica" src="http://uniamerica.31solutions.com/imgs/logo.png"/>
</p>

