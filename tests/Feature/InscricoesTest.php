<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Mail\InscricaoMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InscricoesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function inscreve_na_palestra_e_envia_o_qrcode()
    {
        //Mail::fake();

        $data = [
            'ra' => 501408,
            'email' => 'dnlcorrea@gmail.com',
            'palestra_id' => 1
        ];

        $this->json('POST', '/resources/inscricoes', $data)
            ->dump(200);
        //                    ->assertStatus(200);

        $this->assertDatabaseHas('inscricoes', $data);

        //Mail::assertSent(InscricaoMail::class, function ($mail) use ($data) {
        //   return $mail->hasTo($data['email']);
        //});
    }
}
