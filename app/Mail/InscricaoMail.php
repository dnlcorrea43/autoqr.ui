<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InscricaoMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $inscricao;

    public function __construct ($inscricao)
    {
        $this->inscricao = $inscricao;
    }

    public function build()
    {
        return $this->subject('Inscrição no Evento - QR Code - UniAmérica')
            ->view('mail.inscricao', [
                'inscricao' => $this->inscricao
            ])
            //->attach( storage_path('/app/public/' . $this->inscricao->ra . '.png') )
            ;
    }
}
