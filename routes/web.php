<?php

Route::view('/', 'inscricoes');
Route::view('inscricoes', 'inscricoes');

Route::view('/foo', 'foo');

Route::prefix('resources')
    ->namespace('Resources')
    ->group(function () {
        Route::post('inscricoes', 'InscricoesController@store');
    });
